> Migrated to the monorepo https://bitbucket.org/mvfglobal/rhino-monorepo/src/master/packages/servicer-sqs/

# MVF Servicer SQS

## Usage

TODO

### To install the package

Run `npm install @mvf/servicer-sqs`

### Configuration

Set the following environment variables in your project

- `ENVIRONMENT_FILE` should be set to `development` | `testing` | `staging` | `production`

## Servicer SQS

To setup servicer sqs you will need an `entrypoint.ts` or equivalent file.
```ts
import { Application, DaemonCommand } from "@mvf/servicer";
import { SqsOne } from "@mvf/servicer-sqs";
import { backendConfig } from "./EventSources/Queues/Backend/backendConfig";

const setupApplication = async (): Promise<void> => {
  const application = new Application();
  const sqs = new SqsOne(backendConfig);

  application.addCommands(
    new DaemonCommand(sqs),
  );

  await application.run();
};

void setupApplication();
```

Then needs to setup `backendConfig.ts`

```ts
import { ISqsConfig, sqsConfig } from "@mvf/servicer-sqs";
import { BackendEvents } from "./BackendEvents";

export const backendConfig: ISqsConfig = sqsConfig(
    BackendEvents,
    {
        BACKEND: SQS_BASE_URL + queueName
    },
    "BACKEND"
);
```


## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
