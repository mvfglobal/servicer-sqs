import { mapValues } from 'lodash';
import camelcaseKeys from 'camelcase-keys';
import { MessageAttributeValue } from '@aws-sdk/client-sqs';
import { IPayload } from '@mvf/servicer';
import InvalidPayloadError from './InvalidPayloadError';
import { ISqsMessageHeaders } from './ISqsMessage';
import { ISnsMessageHeaders, ISnsMessageBody } from './ISnsMessage';

interface IHeaders {
  event: string;
  createdAt: number;
  [key: string]: string | number;
}

interface IMessagePayload {
  headers: IHeaders;
  body: IPayload;
}

export interface IMessage {
  Body?: string;
  MessageAttributes?: ISqsMessageHeaders;
}

const extractValue = (header: MessageAttributeValue) => {
  if (header.StringValue) {
    return header.StringValue;
  }

  throw new Error(
    `Unsupported SQS attribute '${header.DataType}' encountered.`,
  );
};

// TODO: changes need testing
const parseSqsHeaders = (headers: ISqsMessageHeaders): IHeaders => ({
  event: 'DEFAULT',
  createdAt: 0,
  ...camelcaseKeys(mapValues(headers, (header) => extractValue(header))),
});

// TODO: changes need testing
const parseSnsHeaders = (headers: ISnsMessageHeaders): IHeaders => ({
  event: 'DEFAULT',
  createdAt: 0,
  ...camelcaseKeys(mapValues(headers, (header) => header.Value)),
});

const isSnsMessage = (
  body: IPayload | ISnsMessageBody,
): body is ISnsMessageBody =>
  !!(
    body.Type &&
    body.Type === 'Notification' &&
    body.Message &&
    body.MessageAttributes
  );

// TODO: Needs to be changed to comply with standards
const parseMessage = (message: IMessage): IMessagePayload => {
  if (message.Body) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const body = JSON.parse(message.Body);
    if (isSnsMessage(body)) {
      return {
        headers: parseSnsHeaders(body.MessageAttributes),
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        body: JSON.parse(body.Message),
      };
    }
    if (message.MessageAttributes) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      return { headers: parseSqsHeaders(message.MessageAttributes), body };
    }
  }
  throw new InvalidPayloadError();
};

export default parseMessage;
