import { MessageAttributeValue } from '@aws-sdk/client-sqs';

export interface ISqsMessageHeaders {
  [key: string]: MessageAttributeValue;
}
