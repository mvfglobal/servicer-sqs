import { IMessage } from './parseMessage';

export const snsMessage: IMessage = {
  Body:
    '{"Type": "Notification","MessageAttributes": {"event": {"Value": "PLATFORM_CAMPAIGN_MERCURY_STATUS_UPDATED","Type": "String"}, "created_at": {"Value": "1","Type": "Number"}, "binary_attribute": {"Value": "1","Type": "Binary"}},"Message": "{\\"key\\": \\"value\\"}"}',
};

export const sqsMessage: IMessage = {
  MessageAttributes: {
    event: {
      StringValue: 'PLATFORM_CAMPAIGN_MERCURY_STATUS_UPDATED',
      DataType: 'String',
    },
    created_at: {
      StringValue: '1',
      DataType: 'Number',
    },
  },
  Body: `${JSON.stringify({ key: 'value' })}`,
};
