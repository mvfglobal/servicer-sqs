import parseMessage from './parseMessage';
import { snsMessage, sqsMessage } from './parseMessage.mock';

describe('MessageParser', () => {
  describe('parsing SQS messages', () => {
    it('should parse header of type string to return string value', () => {
      expect(parseMessage(sqsMessage).headers.event).toEqual(
        'PLATFORM_CAMPAIGN_MERCURY_STATUS_UPDATED',
      );
    });

    it('should parse header of type number to return string value', () => {
      expect(parseMessage(sqsMessage).headers.createdAt).toEqual('1');
    });

    it('should parse body of the message', () => {
      expect(parseMessage(sqsMessage).body).toEqual({ key: 'value' });
    });

    it('should throw an error if message body is missing', () => {
      const sqsMessageWithoutBody = { ...sqsMessage, Body: undefined };
      expect(() => parseMessage(sqsMessageWithoutBody)).toThrowError();
    });

    it('should throw an error if message headers are missing', () => {
      const sqsMessageWithoutHeaders = {
        ...sqsMessage,
        MessageAttributes: undefined,
      };
      expect(() => parseMessage(sqsMessageWithoutHeaders)).toThrowError();
    });
  });

  describe('parsing SNS messages', () => {
    it('should parse header of type string to return value', () => {
      expect(parseMessage(snsMessage).headers.event).toEqual(
        'PLATFORM_CAMPAIGN_MERCURY_STATUS_UPDATED',
      );
    });

    it('should parse header of type number to return value', () => {
      expect(parseMessage(snsMessage).headers.createdAt).toEqual('1');
    });

    it('should parse header of type binary to return value', () => {
      expect(parseMessage(snsMessage).headers.binaryAttribute).toEqual('1');
    });

    it('should parse body of the message', () => {
      expect(parseMessage(snsMessage).body).toEqual({ key: 'value' });
    });

    it('should throw an error if message body is missing', () => {
      const snsMessageWithoutBody = {
        Body:
          '{"Type": "Notification","MessageAttributes": {"event": {"Value": "PLATFORM_CAMPAIGN_MERCURY_STATUS_UPDATED","Type": "String"}, "created_at": {"Value": "1","Type": "Number"}, "binary_attribute": {"Value": "1","Type": "Binary"}}}',
      };
      expect(() => parseMessage(snsMessageWithoutBody)).toThrowError();
    });

    it('should throw an error if message headers are missing', () => {
      const snsMessageWithoutHeaders = {
        Body: `{"Type": "Notification","Message": ${JSON.stringify({
          key: 'value',
        })}}`,
      };
      expect(() => parseMessage(snsMessageWithoutHeaders)).toThrowError();
    });
  });
});
