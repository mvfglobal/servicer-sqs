interface ISnsMessageHeader {
  Value: string;
}

export interface ISnsMessageHeaders {
  [key: string]: ISnsMessageHeader;
}

export interface ISnsMessageBody {
  Type: string;
  MessageAttributes: ISnsMessageHeaders;
  Message: string;
}
