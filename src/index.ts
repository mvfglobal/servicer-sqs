export {
  default as SqsOne,
  Sqs,
} from './SqsOne';
export { default as SqsMany } from './SqsMany';
export { default as sqsConfig } from './sqsConfig';
export { ISqsConfig } from './ISqsConfig';
