import { Consumer } from '@rxfork/sqs-consumer';
import { info, error } from '@mvf/logger';
import { IDaemon } from './IDaemon';
import { ISqsConfig } from './ISqsConfig';

/**
 * Creates an sqs consumer that polls one sqs queue
 */
class SqsOne implements IDaemon {
  static readonly TYPE = 'sqs';

  private consumer: Consumer;

  constructor(private config: ISqsConfig) {
    this.consumer = Consumer.create(config);
    this.consumer.on('error', this.logError);
    this.consumer.on('processing_error', this.logError);
    this.consumer.on('timeout_error', this.logError);
  }

  getName = (): string => `${SqsOne.TYPE}-${this.config.name}`;

  start = (): Promise<void> => {
    this.consumer.start();

    const url = `URL '${this.config.queueUrl}'`;
    const environmentFile = process.env.ENVIRONMENT_FILE ?? 'undefined';
    info(`Sqs consumer ready | ${url} | Environment '${environmentFile}'`);

    return new Promise(() => {});
  };

  private logError = (err: Error) => {
    error(err.message);
  };
}

/**
 * @deprecated Use SqsOne or SqsMany instead
 */
export class Sqs extends SqsOne {};

export default SqsOne;
