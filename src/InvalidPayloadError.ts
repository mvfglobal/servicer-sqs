class InvalidPayloadError extends Error {
  constructor() {
    super('Message is missing headers or body.');
  }
}

export default InvalidPayloadError;
