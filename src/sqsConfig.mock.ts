/* eslint-disable @typescript-eslint/no-empty-function */
import { IAction, IPayload, middleware, next } from '@mvf/servicer';

export enum Queue {
  TEST = 'some queue',
}

const precondition = async function* (payload: IAction<IPayload, IPayload>) {
  yield next(payload);
};

const action = async function* () {};

export const validSource = {
  SOURCE_A: { EVENT_A: middleware(precondition)(action) },
};

export const sourceName = 'SOURCE_A';

export const message = {
  Body: '{}',
  MessageAttributes: { event: { StringValue: 'EVENT_A', DataType: 'String' } },
};

export const invalidEvent = 'EVENT_C';
