import { SqsOne } from '.';
import { IDaemon } from './IDaemon';
import { ISqsConfig } from './ISqsConfig';
import { map } from 'lodash';

/**
 * Creates an sqs consumer that polls many sqs queues
 */
class SqsMany implements IDaemon {
  private sqsQueueConsumers: SqsOne[];

  constructor(configs: ISqsConfig[]) {
    this.sqsQueueConsumers = map(configs, (config) => new SqsOne(config));
  }

  getName = (): string => `${SqsOne.TYPE}`;

  start = (): Promise<void> => {
    void Promise.all(
      map(this.sqsQueueConsumers, (consumer) => consumer.start()),
    );

    return Promise.resolve();
  };
}

export default SqsMany;
