import * as Logger from '@mvf/logger/client';
import * as Exec from '@mvf/servicer-exec/exec';
import { cloneDeep } from 'lodash';
import sqsConfig from './sqsConfig';
import {
  validSource,
  invalidEvent,
  message,
  Queue,
  sourceName,
} from './sqsConfig.mock';

describe('sqsConfig', () => {
  describe('handleMessage function', () => {
    const config = sqsConfig(validSource[sourceName], Queue, 'TEST');

    let spyExec: jest.SpyInstance;
    let warningSpy: jest.SpyInstance;
    let errorSpy: jest.SpyInstance;

    const messageWithoutBody = { ...message, Body: undefined };
    const messageWithoutHeaders = { ...message, MessageAttributes: undefined };

    beforeEach(() => {
      spyExec = jest.spyOn(Exec, 'default');
      warningSpy = jest.spyOn(Logger, 'warning');
      errorSpy = jest.spyOn(Logger, 'error');
    });

    afterEach(() => {
      spyExec.mockRestore();
      warningSpy.mockRestore();
      errorSpy.mockRestore();
    });

    it('should call exec if headers and body are in the message', async () => {
      await config.handleMessage(message);
      expect(spyExec).toHaveBeenCalledTimes(1);
    });

    it('should not call exec if body is missing from the message', async () => {
      await config.handleMessage(messageWithoutBody);
      expect(spyExec).not.toHaveBeenCalled();
    });

    it('should not call exec if headers are missing from the message', async () => {
      await config.handleMessage(messageWithoutHeaders);
      expect(spyExec).not.toHaveBeenCalled();
    });

    it('should throw error if body is missing from the message', async () => {
      await config.handleMessage(messageWithoutBody);
      expect(errorSpy).toHaveBeenCalledWith(
        'Message is missing headers or body.',
      );
    });

    it('should throw error if headers are missing from the message', async () => {
      await config.handleMessage(messageWithoutHeaders);
      expect(errorSpy).toHaveBeenCalledWith(
        'Message is missing headers or body.',
      );
    });

    it('should log warning if invalid event is provided', async () => {
      const messageWithInvalidEvent = cloneDeep(message);
      messageWithInvalidEvent.MessageAttributes.event.StringValue = invalidEvent;

      await config.handleMessage(messageWithInvalidEvent);
      expect(warningSpy).toHaveBeenCalledWith(
        'Invalid event "EVENT_C" provided, valid event list: ["EVENT_A"]',
      );
    });

    it('should rethrow any error thrown in the execution', async () => {
      spyExec.mockImplementation(async () => {
        throw new Error();
      });
      await expect(config.handleMessage(message)).rejects.toThrow();
    });
  });
});
