import { Message } from '@aws-sdk/client-sqs';
import { IEventSource, IPayload } from '@mvf/servicer';
import parseMessage from './parseMessage';
import { ISqsConfig } from './ISqsConfig';
import handleMessageErrors from './sqsLogs';
import exec from "@mvf/servicer-exec/exec";

type Queues = { [key: string]: string };
const sqsConfig = <
  TQueues extends Queues,
  TQueue extends keyof TQueues = keyof TQueues
>(
  source: IEventSource<IPayload, IPayload>,
  queues: TQueues,
  queue: TQueue,
): ISqsConfig => {
  return {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    name: `${queue}`.toLocaleLowerCase().replace('_', '-'),
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    queueUrl: queues[`${queue}`],
    attributeNames: ['SentTimestamp'],
    messageAttributeNames: ['All'],
    waitTimeSeconds: 20,
    handleMessage: async (message: Message): Promise<void> => {
      try {
        const { headers, body } = parseMessage(message);
        await exec(
          headers.event,
          { headers: JSON.stringify(headers), body: JSON.stringify(body) },
          source,
        );
      } catch (err: unknown) {
        if (err instanceof Error) {
          handleMessageErrors(err);
        }
      }
    },
  };
};

export default sqsConfig;
