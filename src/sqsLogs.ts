import { warning, error } from '@mvf/logger';
import { InvalidEventError } from '@mvf/servicer';
import InvalidPayloadError from './InvalidPayloadError';

const handleMessageErrors = (err: Error): void => {
  if (err instanceof InvalidEventError) {
    warning(err.message);
  } else if (err instanceof InvalidPayloadError) {
    error(err.message);
  } else {
    throw err;
  }
};

export default handleMessageErrors;
