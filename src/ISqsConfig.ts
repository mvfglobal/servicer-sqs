import { SQS, Message } from '@aws-sdk/client-sqs';

export interface ISqsConfig {
  name: string;
  queueUrl: string;
  attributeNames: string[];
  messageAttributeNames: string[];
  waitTimeSeconds: number;
  handleMessage: (message: Message) => Promise<void>;
  sqs?: SQS;
}
