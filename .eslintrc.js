module.exports = {
  extends: [
    'airbnb-typescript/base',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.ts', '.tsx'],
      },
    },
  },
  rules: {
    '@typescript-eslint/require-await': 'off',
    'import/extensions': ['error', 'never'],
    'no-void': ['error', { 'allowAsStatement': true }],
    'require-yield': 'off'
  },
  overrides: [
    {
      files: ['*index.ts'],
      rules: {
        'import/prefer-default-export': 'off'
      }
    },
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    project: './tsconfig.json',
  },
};
